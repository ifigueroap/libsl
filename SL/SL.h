/*******************************************************************************

Esta librería utiliza características del estándar ANSI C "C99"
mediante la libreria P99.

En gcc esto se consigue pasando el parametro -std=c99
Las funcionalidades están disponibles desde gcc 4.9

********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#include "p99/p99.h"

#define entrada stdin
#define salida stdout
typedef FILE * archivo;

/* ========= función scan genérica similar al 'leer' SL  ========= */

#define __scan(F, X, I) fscanf((F), _Generic((X)   \
  , char: "%c"                                     \
  , int: "%d"                                      \
  , unsigned int: "%u"    	                   \
  , short int: "%hd"                               \
  , unsigned short int: "%hu"                      \
  , long int: "%ld"                                \
  , unsigned long int: "%lu"                       \
  , float: "%f"					   \
  , double: "%lf"                                  \
  , long double: "%Lf"                             \
  , default: "%f"				   \
), &(X))

#define scan(F, ...)							\
  P99_FOR(F, P99_NARG(__VA_ARGS__), P00_SEQ, __scan, __VA_ARGS__)

/* ========= función imprimir genérica similar a SL  ========= */

#define __print(F, X, I) fprintf((F), "%s", str((X)))

#define print(F, ...)							\
  P99_FOR(F, P99_NARG(__VA_ARGS__), P00_SEQ, __print, __VA_ARGS__)

/* =============================================================================
Conversión genérica de valores a strings.
============================================================================== */

/* El largo de la cadena que va almacenar el numero */
#define LEN_CHAR     (CHAR_BIT * sizeof(char))
#define LEN_INT      (CHAR_BIT * sizeof(int))
#define LEN_UINT     (CHAR_BIT * sizeof(unsigned int))
#define LEN_SINT     (CHAR_BIT * sizeof(short int))
#define LEN_USINT    (CHAR_BIT * sizeof(unsigned short int))
#define LEN_LINT     (CHAR_BIT * sizeof(long int))
#define LEN_ULINT    (CHAR_BIT * sizeof(unsigned long int))
#define LEN_FLOAT    (CHAR_BIT * sizeof(float))
#define LEN_DOUBLE   (CHAR_BIT * sizeof(double))
#define LEN_LDOUBLE  (CHAR_BIT * sizeof(long double))

#define LEN_PTR_INT      (CHAR_BIT * sizeof(int*))

#define str(X) _Generic((X)         \
  , char: char2str                  \
  , char *: str2str                 \
  , int: int2str 	            \
  , unsigned int: uint2str          \
  , short int: sint2str             \
  , unsigned short int: usint2str   \
  , long int: lint2str              \
  , unsigned long int: ulint2str    \
  , float: float2str                \
  , double: double2str              \
  , long double: ldouble2str        \
  , int *: ptr_int2str              \
  , default: double2str	            \
 )(X)

char* char2str(char x) {
  char* str= malloc(LEN_CHAR);
  snprintf(str, LEN_CHAR, "%c", x);
  return str;
}

char* str2str(char *x) {
  return x;
}

char* int2str(int x) {
  char* str= malloc(LEN_INT);
  snprintf(str, LEN_INT, "%d", x);
  return str;
}

char* uint2str(unsigned int x) {
  char* str= malloc(LEN_UINT);
  snprintf(str, LEN_UINT, "%u", x);
  return str;
}

char* sint2str(short int x) {
  char* str= malloc(LEN_SINT);
  snprintf(str, LEN_SINT, "%hd", x);
  return str;
}

char* usint2str(unsigned short int x) {
  char* str= malloc(LEN_USINT);
  snprintf(str, LEN_USINT, "%hu", x);
  return str;
}

char* lint2str(long int x) {
  char* str= malloc(LEN_LINT);
  snprintf(str, LEN_LINT, "%ld", x);
  return str;
}

char* ulint2str(unsigned long int x) {
  char* str= malloc(LEN_ULINT);
  snprintf(str, LEN_ULINT, "%lu", x);
  return str;
}

char* float2str(float x) {
  char* str = malloc(LEN_FLOAT);
  snprintf(str, LEN_FLOAT, "%f", x);
  return str;
}

char* double2str(double x) {
  char* str = malloc(LEN_DOUBLE);
  snprintf(str, LEN_DOUBLE, "%lf", x);
  return str;
}

char* ldouble2str(long double x) {
  char* str = malloc(LEN_LDOUBLE);
  snprintf(str, LEN_LDOUBLE, "%Lf", x);
  return str;
}

char* ptr_int2str(int* x) {
  char* str= malloc(LEN_PTR_INT);
  snprintf(str, LEN_PTR_INT, "%p", x);
  return str;
}

void cls () {
  /* función de compatibilidad con API de SLE 
     no hace nada! */
}
