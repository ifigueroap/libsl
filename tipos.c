#include "SL/SL.h"

/*
Prueba scan/print para
los distintos tipos de datos
*/

int main () {

  char c;
  
  int i;
  unsigned int ui;
  
  short int si;
  unsigned short int sui;

  long int li;
  unsigned long int uli;
  
  float f;
  double ff;
  long double lff;

  scan(entrada, c, i, ui, si, sui, li, uli, f, ff, lff);
  print(salida
	, c, "\n"
	, i, "\n"
	, ui, "\n"
	, si, "\n"
	, sui, "\n"
	, li, "\n"
	, uli, "\n"
	, f, "\n"
	, ff, "\n"
	, lff, "\n");
  
  return 0;
}
